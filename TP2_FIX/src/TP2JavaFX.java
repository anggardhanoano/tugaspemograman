import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.Group;
import javafx.stage.Stage;
import static javafx.geometry.Pos.CENTER;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;

public class TP2JavaFX extends Application {

    public static void main(String[] args) {
        // launch JavaFX
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // init caption
        Text caption = new Text(70, 0, "Happy To See You Smile :)");
        // init face shape and pos
        Circle face = new Circle(150, 100, 90);
        face.setFill(Color.GOLD);
        // init left eye shape and pos
        Circle leftEye = new Circle(110, 80, 10);
        leftEye.setFill(Color.BLACK);
        // init right eye shape and pos
        Circle rightEye = new Circle(190, 80, 10);
        rightEye.setFill(Color.BLACK);
        // init mouth shape for smile shape(default shape)
        Arc mouth = new Arc(150, 120, 45, 35, 0, -180);
        mouth.setFill(Color.GOLD);
        mouth.setStroke(Color.RED);
        mouth.setType(ArcType.OPEN);
        // init left eyebrow shape
        Arc leftBrow = new Arc(110, 70, 25, 10, 0, 180);
        leftBrow.setFill(Color.GOLD);
        leftBrow.setStroke(Color.RED);
        leftBrow.setType(ArcType.OPEN);
        // init right eyebrow shape
        Arc rightBrow = new Arc(190, 70, 25, 10, 0, 180);
        System.out.print(rightBrow);
        rightBrow.setFill(Color.GOLD);
        rightBrow.setStroke(Color.RED);
        rightBrow.setType(ArcType.OPEN);
        // init 4 button for change the face
        Button btn1 = new Button();
        Button btn2 = new Button();
        Button btn3 = new Button();
        Button btn4 = new Button();

        btn1.setText("Smile");
        btn1.setPrefSize(80, 50);
        btn2.setText("Serious");
        btn2.setPrefSize(80, 50);
        btn3.setText("Sad");
        btn3.setPrefSize(80, 50);
        btn4.setText("Laugh");
        btn4.setPrefSize(80, 50);
        // settings action when button 1 pressed --> smile
        btn1.setOnAction(event -> {
            mouth.setRadiusY(35);
            mouth.setCenterY(120);
            mouth.setStartAngle(0);

            leftBrow.setRadiusY(10);
            leftBrow.setStartAngle(0);
            rightBrow.setRadiusY(10);
            rightBrow.setStartAngle(0);
            mouth.setFill(Color.GOLD);

            caption.setText("Happy To See You Smile :)");
        });
        // settings action when button 2 pressed --> poker
        btn2.setOnAction(event -> {
            mouth.setRadiusY(0);
            mouth.setCenterY(140);

            leftBrow.setRadiusY(0);
            leftBrow.setStartAngle(0);
            rightBrow.setRadiusY(0);
            rightBrow.setStartAngle(0);

            caption.setText("Don't Forget To Smile :)");
        });
        // settings action when button 3 pressed --> sad
        btn3.setOnAction(event -> {
            mouth.setRadiusY(35);
            mouth.setCenterY(155);
            mouth.setStartAngle(-180);

            leftBrow.setRadiusY(10);
            leftBrow.setStartAngle(-20);
            rightBrow.setRadiusY(10);
            rightBrow.setStartAngle(20);
            mouth.setFill(Color.GOLD);

            caption.setText("Please Don't Be Sad :(");

        });
        // settings action when button 4 pressed --> laugh
        btn4.setOnAction(event -> {
            mouth.setRadiusY(35);
            mouth.setCenterY(120);
            mouth.setStartAngle(0);

            leftBrow.setRadiusY(20);
            leftBrow.setStartAngle(0);
            rightBrow.setRadiusY(20);
            rightBrow.setStartAngle(0);
            mouth.setFill(Color.RED);

            caption.setText("You Are So Funny :D");
        });
        // horizontal container for wrap all button in horizontal alligment
        HBox HContainer = new HBox(btn1, btn2, btn3, btn4);
        HContainer.setAlignment(CENTER);
        // wrap all node into the container except HContainer
        Group container = new Group(face, leftEye, rightEye, mouth, leftBrow, rightBrow, caption);
        // wrap the HContainer and the container to the VBox
        VBox root = new VBox(10, container, HContainer);
        root.setAlignment(CENTER);
        // init the scene object
        Scene scene = new Scene(root, 300, 250);
        primaryStage.setTitle("Show Your Expression");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
