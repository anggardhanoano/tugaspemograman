import javafx.animation.PathTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.util.Duration;
import javafx.scene.text.Text;

public class TP3JavaFX extends Application {
    static boolean isPlay = true;

    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {
        // Create a pane, a rectangle, and a circle
        Pane pane = new Pane();
        Circle ball = new Circle(0, 0, 20);
        ball.setFill(Color.GREEN);
        Circle circle = new Circle(125, 100, 50);
        circle.setFill(Color.WHITE);
        circle.setStroke(Color.BLACK);
        // cosinus Line
        Polyline cosLine = new Polyline();
        for (double angle = -620.0; angle <= 620.0; angle++) {
            cosLine.getPoints().addAll(angle, -(Math.cos(Math.toRadians(angle))));
        }
        cosLine.setTranslateY(300);
        cosLine.setTranslateX(300);
        cosLine.setScaleX(0.4);
        cosLine.setScaleY(100);
        cosLine.setStrokeWidth(1.0 / 25);

        // cartesius line

        // for y
        Line lineY = new Line(300, 50, 300, 550);
        Line arrowY1 = new Line(290, 55, 300, 50);
        Line arrowY2 = new Line(310, 55, 300, 50);
        Line arrowY3 = new Line(290, 540, 300, 550);
        Line arrowY4 = new Line(310, 540, 300, 550);

        // for x
        Line lineX = new Line(30, 300, 570, 300);
        Line arrowX1 = new Line(30, 300, 40, 310);
        Line arrowX2 = new Line(30, 300, 40, 290);
        Line arrowX3 = new Line(570, 300, 560, 290);
        Line arrowX4 = new Line(570, 300, 560, 310);
        // make the label
        Text minus2Phi = new Text("-2\u03C0");
        minus2Phi.setX(setCoorX(2.0, 500));
        minus2Phi.setY(310);

        Text minusPhi = new Text("-\u03C0");
        minusPhi.setX(setCoorX(3.0, 500));
        minusPhi.setY(310);

        Text positive2Phi = new Text("2\u03C0");
        positive2Phi.setX(setCoorX(6.0, 500));
        positive2Phi.setY(310);

        Text positivePhi = new Text("\u03C0");
        positivePhi.setX(setCoorX(5.0, 500));
        positivePhi.setY(310);
        // put all node to the pane
        pane.getChildren().addAll(cosLine, ball, lineX, lineY, arrowY1, arrowY2, arrowY3, arrowY4, arrowX1, arrowX2,
                arrowX3, arrowX4, minus2Phi, minusPhi, positive2Phi, positivePhi);

        // Create a path transition
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(4000));
        pt.setPath(cosLine);
        pt.setNode(ball);
        pt.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
        pt.setCycleCount(Timeline.INDEFINITE);
        pt.setAutoReverse(true);
        pt.play(); // Start animation
        pane.setOnMouseClicked(e -> {
            if (isPlay) {
                pt.pause();
                isPlay = false;
            } else {
                pt.play();
                isPlay = true;
            }
        });

        // Create a scene and place it in the stage
        Scene scene = new Scene(pane, 600, 600);
        primaryStage.setTitle("TP3: Animasi Bola pada Kurva"); // Set the stage title
        primaryStage.setScene(scene); // Place the scene in the stage
        primaryStage.show(); // Display the stage
    }

    public static void main(String[] args) {
        launch(args);
    }

    static double setCoorX(double part, int CONST) {
        double result = part / 7 * CONST;
        System.out.println(result);
        return result;
    }
}