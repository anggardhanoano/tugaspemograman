import java.util.*;

class PostFixForm {
    static Stack<String> tempOperator = new Stack<String>();
    static Map<String, Integer> dictionary = new HashMap<String, Integer>();
    static String postFixResult = "";

    public PostFixForm(String rawInput) {
        dictionary.put("+", 1);
        dictionary.put("-", 1);
        dictionary.put("*", 2);
        dictionary.put("/", 2);
        dictionary.put("$", 3);
        dictionary.put("(", 4);
        dictionary.put(")", 4);

        String modifiedInput = rawInput;
        setPostFix(modifiedInput);
        setResult();
    }

    /**
     * @param input for infix input from the user
     */
    static void setPostFix(String input) {
        StringTokenizer inputToken = new StringTokenizer(input);
        while (inputToken.hasMoreTokens()) {

            String character = inputToken.nextToken();

            if (dictionary.get(character) == null) {
                postFixResult += character + " ";
            } else {
                if (!tempOperator.empty()) {

                    int cases = cekPrecedence(character, tempOperator.peek());

                    switch (cases) {
                    // if the operator precedence from outside stack smaller than in the stack
                    case 1:
                        if (tempOperator.peek().equals("(") || tempOperator.peek().equals(")")) {
                            // pop out the parenthesis
                            if (tempOperator.peek().equals(")")) {
                                /*
                                 * pop out the operator after the parenthesis before push new operator to stack
                                 */
                                tempOperator.pop();
                                postFixResult += tempOperator.pop() + " ";
                            } else {
                                tempOperator.pop();
                            }
                        } else {
                            postFixResult += tempOperator.pop() + " ";
                        }
                        tempOperator.push(character);
                        break;
                    // if the operator precedence from outside stack bigger than in the stack
                    case 2:
                        tempOperator.push(character);
                        break;
                    // if the operator precedence from outside stack equal with in the stack
                    case 3:
                        if (character.equals("$")) {
                            tempOperator.push(character);
                        } else if (character.equals("(") || character.equals(")")) {
                            tempOperator.pop();
                        } else {
                            postFixResult += tempOperator.pop() + " ";
                            tempOperator.push(character);
                            break;
                        }
                    }
                } else {
                    tempOperator.push(character);
                }
            }
        }

    }

    /**
     * @param operator1 for the operator from the input
     * @param operator2 for the operator from stack
     */
    static int cekPrecedence(String operator1, String operator2) {
        if (dictionary.get(operator1) == dictionary.get(operator2)) {
            return 3;
        } else if (dictionary.get(operator1) > dictionary.get(operator2)) {
            return 2;
        } else {
            return 1;
        }
    }

    // retrieve the postfix result until the tempOperator stack empty
    static void setResult() {
        while (!tempOperator.empty()) {
            if (tempOperator.peek().equals("(") || tempOperator.peek().equals(")")) {
                tempOperator.pop();
            } else {
                postFixResult += tempOperator.pop() + " ";
            }
        }
    }

    // get the result
    public String getResult() {
        String result = postFixResult;
        postFixResult = "";
        return result;
    }

}