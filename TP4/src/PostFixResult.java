import java.util.*;
import java.util.regex.Pattern;

class PostFixResult {

    static Stack<Long> integerKeeper = new Stack<Long>();
    static long result = (long) 0;
    String rawPostFix;

    PostFixResult(String formedPostFix) throws PostFixException {

        Pattern Regex = Pattern.compile("[0123456789\\s]");
        Pattern Operator = Pattern.compile("[+-/*$]");

        if (!Operator.matcher(formedPostFix).find()) {
            throw new PostFixException("Don't forget to input the operator");
        } else {

            PostFixForm postFix = new PostFixForm(formedPostFix);
            rawPostFix = postFix.getResult();
            StringTokenizer inputToken = new StringTokenizer(rawPostFix);

            while (inputToken.hasMoreTokens()) {

                String character = inputToken.nextToken();
                if (PostFixForm.dictionary.get(character) == null) {
                    if (Regex.matcher(character).find())
                        try {
                            integerKeeper.push(Long.parseLong(character));
                        } catch (NumberFormatException error) {
                            throw new PostFixException("Please use blank space");
                        }
                    else {
                        throw new PostFixException("please input number and operator only");
                    }
                } else {
                    try {
                        long firstStack = integerKeeper.pop();
                        long secondStack = integerKeeper.pop();
                        calculate(secondStack, firstStack, character);
                    } catch (EmptyStackException error) {
                        throw new PostFixException("are you sure the input is correct?");
                    }
                }
            }
        }
        setResult();
    }

    /**
     * 
     * @param operand1 for the second position in the stack
     * @param operand2 for the first position in the stack
     * @param operator for identify the operator
     */
    static void calculate(long operand1, long operand2, String operator) throws PostFixException {

        switch (operator) {
        case "+":
            integerKeeper.push(operand1 + operand2);
            break;
        case "-":
            integerKeeper.push(operand1 - operand2);
            break;
        case "*":
            integerKeeper.push(operand1 * operand2);
            break;
        case "/":
            if (operand2 == 0) {
                throw new PostFixException("Error zero division");
            } else {
                integerKeeper.push(operand1 / operand2);
            }
            break;
        case "$":
            long temp = (long) Math.pow((double) operand1, (double) operand2);
            integerKeeper.push(temp);
            break;
        }
    }

    static void setResult() {
        result = integerKeeper.pop();
    }

    public long getResult() {
        return result;
    }
}