import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;

class PostFixException extends Exception {
    public PostFixException(String e) {
        Alert error = new Alert(AlertType.NONE);
        error.setAlertType(AlertType.ERROR);
        error.setContentText(e);
        error.show();
    }
}