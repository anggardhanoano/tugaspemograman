import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.geometry.Insets;
import static javafx.geometry.Pos.CENTER;
import javafx.stage.Stage;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import java.util.*;
import java.util.regex.Pattern;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws PostFixException {
        /*
         * make 3 component, TextField, result text and postFix form text
         */

        GridPane root = new GridPane();
        root.setAlignment(CENTER);

        // initialze scene
        Scene scene = new Scene(root, 500, 150);

        root.setPadding(new Insets(10, 25, 10, 25));
        root.setVgap(10);
        root.setHgap(10);

        Text inputText = new Text("Infix Expression :");
        root.add(inputText, 0, 1);

        TextField input = new TextField();
        input.setPrefWidth(300);
        root.add(input, 1, 1);

        Text postFixLabel = new Text("PostFix Expression :");
        root.add(postFixLabel, 0, 2);

        Text postFixForm = new Text("-");
        root.add(postFixForm, 1, 2);

        Text resultLabel = new Text("Result :");
        root.add(resultLabel, 0, 3);

        Text postFixResult = new Text("-");
        root.add(postFixResult, 1, 3);

        input.setOnAction(event -> {
            try {
                if (input.getText().equals("")) {
                    throw new PostFixException("please input the operation");
                } else {
                    PostFixResult inputObject = new PostFixResult(processInput(input.getText()));
                    input.clear();
                    postFixResult.setText(inputObject.getResult() + "");
                    postFixForm.setText(inputObject.rawPostFix);
                }
            } catch (PostFixException error) {
                input.clear();
            }
        });

        primaryStage.setTitle("PostFix Calculator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /*
     * processInput will return a string where the input from user will be processed
     * for the adjustment with tokenizer
     */
    public static String processInput(String input) throws PostFixException {
        Stack<String> rawInput = new Stack<String>();
        Pattern Regex = Pattern.compile("[0123456789]");
        int openCount = 0;
        int closeCount = 0;
        String processedInput = "";
        char[] inputArray = input.replaceAll(" ", "").toCharArray();
        String tempStringLast = "";

        for (char c : inputArray) {
            // check the sum of '(' and ')', if the sum is not equal than return error
            if (c == '(') {
                openCount++;
            }
            if (c == ')') {
                closeCount++;
            }

            /*
             * if the character c is in the regex than push to the stack, but if not then
             * add to the processedInput with a space. this method to handle second digit
             * issue and random space input from user
             */
            if (Regex.matcher(c + "").find()) {
                rawInput.push(c + "");
            } else {
                if (!rawInput.isEmpty()) {

                    String tempString = "";
                    while (!rawInput.isEmpty()) {
                        tempString += rawInput.pop();
                    }
                    processedInput += reverseString(tempString);
                    processedInput += " " + c + " ";
                } else {
                    processedInput += c + " ";
                }
            }
        }
        /*
         * pop all number in the stack which is this number is the last number in
         * operation
         */
        while (!rawInput.isEmpty()) {
            tempStringLast += rawInput.pop();
        }
        processedInput += reverseString(tempStringLast);

        if (openCount != closeCount) {
            throw new PostFixException("cek your parenthesis!");
        }
        return processedInput.trim();
    }

    /*
     * for handle the reversed number when pop from the stack, this happen because
     * the top of the stack is the last digit and the lowest stack is the first
     * digit
     */
    public static String reverseString(String theString) {
        StringBuilder reversedString = new StringBuilder(theString);
        return reversedString.reverse() + "";
    }
}