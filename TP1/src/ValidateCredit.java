import javax.swing.JOptionPane;

// Anggardha Febriano 1806235800

/**
 * @author anggardhanoano
 */
public class ValidateCredit {
    public static void main(String[] args){

        boolean run = true;

        while (run){
            try {
                String Input = JOptionPane.showInputDialog(null,
                        "Enter a credit number,\nQUIT to end:",
                        "Validation of Credit Card/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);
                String isValid;
                if (Input.toUpperCase().equals("QUIT")){
                    run = false;
                }else if (Input.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please enter the credit card number",
                            "Blank Form", JOptionPane.PLAIN_MESSAGE);
                }else {
                    // if the input contain whitespace,erase all of it
                    long numberInput = Long.valueOf(Input.replaceAll(" ",""));
                    if (isValid(numberInput)){
                        isValid = "Valid";
                    }else {
                        isValid = "Invalid";
                    }
                    JOptionPane.showMessageDialog(null,
                            "The Number " + numberInput + " is " + (isValid(numberInput) ? "Valid" : "Invalid") + "\n"
                            , "Validation of Credit Card/Debit Card Numbers", JOptionPane.PLAIN_MESSAGE);
                }
            // catch for all possible error
            }catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(null, "Error :(\nare you sure input  Integer?",
                        "Error", JOptionPane.PLAIN_MESSAGE);
            }catch (StringIndexOutOfBoundsException ex){
                JOptionPane.showMessageDialog(null, "Error :(\nare you sure that's the right number?",
                        "Error", JOptionPane.PLAIN_MESSAGE);
            }catch (NullPointerException ex){
                run = false;
            }

        }
    }

    // get the sum of even index
    // read from left to the right
    public static int sumOfDoubleEven(long number){
        int i = 0;  // if the size is even, then the first digit is even
        if (getSize(number) % 2 != 0){
                i = 1;
        }
        int result = 0;
        String code = number+"";

        while (i < getSize(number)){
            int temp = Character.getNumericValue(code.charAt(i)) * 2;
            int cekResult = getDigit(temp);
            result += cekResult;
            i+=2;
        }
        return result;
    }
    // cek the digit of even number after * 2, then if more than 1 digit,sum the digit
    public static int getDigit(int number){

        if (number > 9){
             return number % 10 + 1;
        }
        return number;
    }

    // get the sum of odd index
    public static int sumOfOddPlace(long number){

        int i = 1;  // if the size is odd, then the first index is odd
        if (getSize(number) % 2 != 0){
            i = 0;
        }
        int result = 0;
        String code = number+"";

        while (i < getSize(number)){
            int num = Character.getNumericValue(code.charAt(i));
            result+=num;
            i+=2;
        }
        return result;
    }

    // all validation required are handled in this method
    public static boolean isValid( long number){

        int even = sumOfDoubleEven(number);
        int odd = sumOfOddPlace(number);
        int total = even + odd;
        boolean result = false;
        boolean prefix =    prefixMatched(number, 4) ||
                            prefixMatched(number, 5) ||
                            prefixMatched(number, 6) ||
                            prefixMatched(number, 37);
        boolean size = getSize(number) >=13 && getSize(number) <= 16;

        if (size && prefix && total % 10 == 0){
          result = true;
        }
        return result;
    }

    // cek the prefix with the number we want, in this case 4,5,6, & 37
    public static boolean prefixMatched(long number, int d){

        boolean cek = false;
        long temp = getPrefix(number, getSize(d));

        if (temp == d){
            cek = true;
        }
        return cek;
    }
    // get the length of credit card number
    public static int getSize (long number){

        String temp = number+"";    // convert the credit number into string again
        int size = temp.length();
        return size;
    }

    /*get the prefix number from credit card number, if credit card number digit > k
     then we slice the number from index 0 to index k
     if digit credit card number < k then return credit card number */

    /**
     * Get prefix from number
     * @param number source number
     * @param k length prefix
     * @return prefix
     */
    public static long getPrefix(long number, int k){

        String temp = number+"";
        long result = Long.parseLong(temp.substring(0,k));

        if(temp.length() <= k){
            return number;
        }
        return result;
    }
}
